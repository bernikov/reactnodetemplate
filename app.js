let port = process.env.PORT || 8000,
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

const path = require('path');

const redirect = (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client/build', 'index.html'))
};


if (process.env.NODE_ENV === "production") {
    app.use(express.static("client/build"));
}

/**
 * Routes
 */
let homeRoute = require('./routes/home')

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/home', homeRoute);

app.get('*', redirect);

app.listen(port, function () {
    console.log("Server running in port %d ", port);
});


