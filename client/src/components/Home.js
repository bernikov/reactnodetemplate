import React, {Component} from 'react';
import {Helmet} from "react-helmet";

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: "",
            isLoading: false
        }
    }

    componentWillMount() {
        this.fetchSampleHome()
    }

    fetchSampleHome = () => {
        this.setState({isLoading: true});

        fetch('/api/home')
            .then(response => response.json())
            .then(response => {
                let responseJson = response.data;

                this.setState({data: responseJson})
            })
            .catch((error) => {
                console.error(error);
            })
            .then(() => {
                this.setState({isLoading: false});
            });
    };

    render() {
        return (
            <div className='center page-container'>
                <Helmet>
                    <title>Template | Fidku</title>
                </Helmet>
                <div className="col-md-12 center">{this.state.data}</div>
            </div>
        );
    }
}

export default Home;
