import React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap'
import '../../main.css';
import {NavLink} from "react-router-dom";

class WebNavBar extends React.Component {
    render() {
        return (
            <div>
                <Navbar collapseOnSelect fixedTop>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <NavLink to="/">
                                <img alt="img1" className='logo desktop' src={require('../../assets/fidku.png')}/>
                                <img alt="img2"  className='logo mobile' src={require('../../assets/fidku.png')}/>
                            </NavLink>
                        </Navbar.Brand>
                        <Navbar.Toggle/>
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            <NavItem eventKey={1} componentClass="div">
                                <NavLink to="/">Inicio</NavLink>
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}

export default WebNavBar
