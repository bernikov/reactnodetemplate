import AppRouter from './AppRouter';
import registerServiceWorker from './registerServiceWorker';
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

render((
    <BrowserRouter>
        <AppRouter />
    </BrowserRouter>
), document.getElementById('root'));

registerServiceWorker();