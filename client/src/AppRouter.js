import React from 'react';
import WebNavBar from './components/layout/WebNavBar'
import WebFooter from './components/layout/WebFooter'
import './main.css';
import Main from "./Main";

class AppRouter extends React.Component {
  render() {
    return (
      <div>
        <WebNavBar />
          <Main />
        <WebFooter />
      </div>
    )
  }
}

export default AppRouter
