const express = require('express'),
    router = express.Router(),
    homeCtrl = require('../controllers/home');

/**
 *  Routes
 */
router.route('/')
    .get(homeCtrl.list, function (req, res) {});

module.exports = router;
