let model = require('../models/home');

exports.list = (req, res, next) => {

    model.list((err, data) => {
        res.status(err ? 503 : 200).json({
            error: err ? true : null,
            errorMessage: err ? err : null,
            data: data
        });
    });
};
